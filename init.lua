-- Boilerplate to support localized strings if intllib mod is installed.
local S
if minetest.get_modpath("intllib") then
	S = intllib.Getter()
else
	S = function(s,a,...)a={a,...}return s:gsub("@(%d+)",function(n)return a[tonumber(n)]end)end
end

--[[ privileges ]]
if(minetest.setting_getbool("enable_damage") == true) then
	minetest.register_privilege("heal", {
		description = S("Allows player to set own health and breath with /sethp, /sethealth and /setbreath"),
		give_to_singleplayer = false
	})
end
minetest.register_privilege("physics", {
	description = S("Allows player to set own gravity, jump height and movement speed with /setgravity, /setjump and /setspeed, respectively"),
	give_to_singleplayer = false
})
minetest.register_privilege("hotbar", {
	description = S("Allows player to set the number of slots of the hotbar with /sethotbarsize"),
	give_to_singleplayer = false
})

--[[ informational commands ]]
minetest.register_chatcommand("whoami", {
	params = "",
	description = S("Shows your player name"),
	privs = {},
	func = function(name)
		minetest.chat_send_player(name, S("Your player name is @1.", name))
	end,
})

minetest.register_chatcommand("ip", {
	params = "",
	description = S("Shows your IP address"),
	privs = {},
	func = function(name)
		minetest.chat_send_player(name, S("Your IP address is @1.", minetest.get_player_ip(name)))
	end
})

--[[ HUD commands ]]
minetest.register_chatcommand("sethotbarsize", {
	params = S("<1...23>"),
	description = S("Sets the size of your hotbar to the provided number of slots; the number must be between 1 and 23"),
	privs = {hotbar=true},
	func = function(name, slots)
		if slots == "" then
			minetest.chat_send_player(name, S("You did not specify the parameter."))
			return
		end
		if type(tonumber(slots)) ~= "number" then
			minetest.chat_send_player(name, S("This is not a number."))
			return
		end
		if tonumber(slots) < 1 or tonumber(slots) > 23 then
			minetest.chat_send_player(name, S("Number of slots is out of bounds. The number of slots must be between 1 and 23."))
			return
		end
		local player = minetest.get_player_by_name(name)
		player:hud_set_hotbar_itemcount(tonumber(slots))
	end,
})

--[[ health and breath commands ]]

if(minetest.setting_getbool("enable_damage") == true) then
	minetest.register_chatcommand("sethp", {
		params = S("<hp>"),
		description = S("Sets your health to <hp> HP (=half the number of “hearts”)"),
		privs = {heal=true},
		func = function(name, hp)
			local player = minetest.get_player_by_name(name)
			if not player then
				return
			end
			if hp == "" then
				minetest.chat_send_player(name, S("You did not specify the parameter."))
				return
			end
			if type(tonumber(hp)) ~= "number" then
				minetest.chat_send_player(name, S("This is not a number."))
				return
			end
			hp = math.max(0, math.floor(hp+0.5)) -- round the number, ensure minimum value of 0
			player:set_hp(tonumber(hp))
		end,
	})

	minetest.register_chatcommand("sethealth", {
		params = S("<hearts>"),
		description = S("Sets your health to <hearts> hearts"),
		privs = {heal=true},
		func = function(name, hearts)
			local player = minetest.get_player_by_name(name)
			if not player then
				return
			end
			if hearts == "" then
				minetest.chat_send_player(name, S("You did not specify the parameter."))
				return
			end
			if type(tonumber(hearts)) ~= "number" then
				minetest.chat_send_player(name, S("This is not a number."))
				return
			end
			local hp = tonumber(hearts) * 2
			hp = math.max(0, math.floor(hp+0.5)) -- round the number, ensure minimum value of 0
			player:set_hp(hp)
		end,
	})

	minetest.register_chatcommand("setbreath", {
		params = S("<breath>"),
		description = S("Sets your breath to <breath> breath points"),
		privs = {heal=true},
		func = function(name, breath)
			local player = minetest.get_player_by_name(name)
			if not player then
				return
			end
			if breath == "" then
				minetest.chat_send_player(name, S("You did not specify the parameter."))
				return
			end
			if type(tonumber(breath)) ~= "number" then
				minetest.chat_send_player(name, S("This is not a number."))
				return
			end
			local bp = math.max(0, tonumber(breath)) -- ensure minimum value of 0
			player:set_breath(bp)
		end,
	})

	minetest.register_chatcommand("killme", {
		params = "",
		description = S("Kills yourself"),
		func = function(name, param)
			local player = minetest.get_player_by_name(name)
			if not player then
				return
			end
			player:set_hp(0)
		end,
	})
end

--[[ Player physics commands ]]

-- speed
minetest.register_chatcommand("setspeed", {
	params = S("[<speed>]"),
	description = S("Sets your movement speed to <speed> (default: 1)"),
	privs={physics=true},
	func = function(name, speed)
		local player = minetest.get_player_by_name(name)
		if not player then
			return
		end
		if speed == "" then
			speed=1
		end
		if type(tonumber(speed)) ~= "number" then
			minetest.chat_send_player(name, S("This is not a number."))
			return
		end

		player:set_physics_override(tonumber(speed), nil, nil)
	end,
})

-- gravity
minetest.register_chatcommand("setgravity", {
	params = S("[<gravity>]"),
	description = S("Sets your gravity to <gravity> (default: 1)"),
	privs={physics=true},
	func = function(name, gravity)
		local player = minetest.get_player_by_name(name)
		if not player then
			return
		end
		if gravity == "" then
			gravity=1
		end
		if type(tonumber(gravity)) ~= "number" then
			minetest.chat_send_player(name, S("This is not a number."))
			return
		end
		player:set_physics_override(nil, tonumber(gravity), nil)
	end,
})

-- jump height
minetest.register_chatcommand("setjump", {
	params = S("[<jump height>]"),
	description = S("Sets your jump height to <jump height> (default: 1)"),
	privs = {physics=true},
	func = function(name, jump_height)
		local player = minetest.get_player_by_name(name)
		if not player then
			return
		end
		if jump_height == "" then
			jump_height=1
		end
		if type(tonumber(jump_height)) ~= "number" then
			minetest.chat_send_player(name, S("This is not a number."))
			return
		end
		player:set_physics_override(nil, nil, jump_height)
	end,
})

minetest.register_chatcommand("pulverizeall", {
	params = "",
	description = S("Destroys all items in your player inventory and crafting grid"),
	func = function(name, param)
		local player = minetest.get_player_by_name(name)
		if not player then
			return
		end
		local inv = player:get_inventory()
		inv:set_list("main", {})
		inv:set_list("craft", {})
	end,
})

